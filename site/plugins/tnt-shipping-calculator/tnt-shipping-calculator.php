<?php
/*
Plugin Name: T N T-Shipping-Calculator
Plugin URI: http://agilesolutionspk.com
Description: T N T-Shipping-Calculator
Version: 1.2
Author: agilesolutionspk.com
Author URI: http://agilesolutionspk.com
*/
 
/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
	function WC_Aspk_TNT_init() {
		if ( ! class_exists( 'WC_Aspk_TNT' ) ) {
			class WC_Aspk_TNT extends WC_Shipping_Method {
				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct() {
					$this->id                 = 'aspk_tnt_shipping'; // Id for your shipping method. Should be uunique.
					$this->method_title       = __( 'ASPK TNT' );  // Title shown in admin
					$this->method_description = __( 'Shipping method for TNT RTT' ); // Description shown in admin
 
					$this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
					
					$this->init();
				}
 
				/**
				 * Init your settings
				 *
				 * @access public
				 * @return void
				 */
				function init() {
					// Load the settings API
					$this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
					$this->init_settings(); // This is part of the settings API. Loads settings you previously init.
 
					// Define user set variables
					$this->title 		  = $this->get_option( 'title' );
					$this->accountno 		  = $this->get_option( 'accountno' );
					$this->username 		  = $this->get_option( 'username' );
					$this->password 		  = $this->get_option( 'password' );
					
					// Save settings in admin if you have any defined
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}
				
				/**
				 * Initialise Gateway Settings Form Fields
				 *
				 * @access public
				 * @return void
				 */
				function init_form_fields() {

					$this->form_fields = array(
						'enabled' => array(
										'title' 		=> __( 'Enable/Disable', 'woocommerce' ),
										'type' 			=> 'checkbox',
										'label' 		=> __( 'Enable this shipping method', 'woocommerce' ),
										'default' 		=> 'no',
									),
						'title' => array(
										'title' 		=> __( 'Method Title', 'woocommerce' ),
										'type' 			=> 'text',
										'description' 	=> __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
										'default'		=> __( 'ASPK TNT', 'woocommerce' ),
										'desc_tip'		=> true
									),
						'accountno' => array(
										'title' 		=> __( 'Account No.', 'woocommerce' ),
										'type' 			=> 'text',
										'description' 	=> __( 'Please enter your TNT account number', 'woocommerce' ),
										'default'		=> __( '000', 'woocommerce' ),
										'desc_tip'		=> true
									),			
						'username' => array(
										'title' 		=> __( 'User Name', 'woocommerce' ),
										'type' 			=> 'text',
										'description' 	=> __( 'TNT User Name', 'woocommerce' ),
										'default'		=> __( 'CIT00000000000055444', 'woocommerce' ),
										'desc_tip'		=> true
									),			
						'password' => array(
										'title' 		=> __( 'Password', 'woocommerce' ),
										'type' 			=> 'text',
										'description' 	=> __( 'TNT Password', 'woocommerce' ),
										'default'		=> __( 'Enter password', 'woocommerce' ),
										'desc_tip'		=> true
									),			
						);

				}
				function collection_shipping_add($target_area){
					$collect_address = array();
					if($target_area == 'VIC' || $target_area == 'TAS'){
						$collect_address['suburb'] = 'Melbourne';
						$collect_address['postcode'] =  '3000';
						$collect_address['state'] = 'VIC';
						return $collect_address;
					}elseif($target_area == 'WA' || $target_area == 'NT'){
						$collect_address['suburb'] = 'Perth';
						$collect_address['postcode'] =  '6017';
						$collect_address['state'] = 'WA';
						return $collect_address;
					}elseif($target_area == 'SA' || $target_area == 'NSW' || $target_area == 'qld' ){
						$collect_address['suburb'] = 'Sydney';
						$collect_address['postcode'] =  '2128';
						$collect_address['state'] = 'SA';
						return $collect_address;
					}
				}
			function prepare_tnt_req($package){
				$deliver_address = $package['destination'];
				//$deliver_city = $this->getFirstTown( $deliver_address['postcode'] );
				$collect_address = $this->collection_shipping_add($deliver_address['state'] );
				$request = new SimpleXMLElement('<enquiry/>');
				$request->addAttribute('xmlns', 'http://www.tntexpress.com.au');//TODO check url

				$rated = $request->addChild('ratedTransitTimeEnquiry');
				$cutoff = $rated->addChild('cutOffTimeEnquiry');

				$col = $cutoff->addChild('collectionAddress');
				$col->addChild('suburb', $collect_address['suburb']);
				$col->addChild('postCode', $collect_address['postcode']);
				$col->addChild('state', $collect_address['state']);

				$del = $cutoff->addChild('deliveryAddress');
				$del->addChild('suburb', $deliver_address['city']);
				$del->addChild('postCode', $deliver_address['postcode']);
				$del->addChild('state', $deliver_address['state']);
				$cutoff->addChild('shippingDate',date('Y-m-d',time()+(86400 * 2)));
				$cutoff->addChild('userCurrentLocalDateTime', date('c'));

				$dangerous = $cutoff->addChild('dangerousGoods');
				$dangerous->addChild('dangerous', 'false');

				$lines = $cutoff->addChild('packageLines');
				$lines->addAttribute('packageType', 'N');
				foreach ($package['contents'] as $shipping) { 
					$prod_id = $shipping['product_id'];
					$prod_post_meta = get_post_meta($prod_id);
					$line = $lines->addChild('packageLine');
					$line->addChild('numberOfPackages', '1'); 
					$dims = $line->addChild('dimensions');
					$dims->addAttribute('unit', 'cm');
					$dims->addChild('length', $prod_post_meta['_length'][0]);
					$dims->addChild('width', $prod_post_meta['_width'][0]);
					$dims->addChild('height', $prod_post_meta['_height'][0]);
					$weight = $line->addChild('weight');
					$weight->addAttribute('unit', 'kg');
					$weight->addChild('weight', $prod_post_meta['_weight'][0]);
				}

				$payment = $rated->addChild('termsOfPayment');
				$payment->addChild('senderAccount', $this->accountno);
				$payment->addChild('payer', 'S');

				return array(
					'username' => $this->username,
					'password' => $this->password,
					'version' => 2,
					'xmlRequest' => $request->asXML(),
				);
			}
			function send_tnt_req($req){
				$url = 'https://www.tntexpress.com.au/Rtt/inputRequest.asp';
				$para = array(
					'method' => 'POST',
					'timeout' => 45,
					'redirection' => 5,
					'httpversion' => '1.0',
					'blocking' => true,
					'headers' => array(),
					'body' => $req,
					'cookies' => array(),
					'sslverify' => false
				);
				$response = wp_remote_post( $url, $para);
				if ( is_wp_error( $response ) ) {
				   $error_message = $response->get_error_message();
				   echo "Something went wrong: $error_message";
				}
				return $response;
			}
				// get city for client
				public function getLocations( $cachedRequested = true ){
					if( !$cachedRequested )
						return $this->_locationsToArray();
					else
					{
						if( file_exists( $this->locationsCacheFile ) )
						{
							// Return cached data if not expired
							$fileAge = time() - filemtime( $this->locationsCacheFile );
							if( $fileAge < $this->cacheTTL * 3600 )
								$locationsList = unserialize(file_get_contents($this->locationsCacheFile));
						}
						// Either expired or doesn't exist
						if( !isset( $locationsList ) )
						{
							$locationsList = $this->_locationsToArray();
							file_put_contents( $this->locationsCacheFile, serialize( $locationsList ) );
						}
						return $locationsList;
					}
				}
				public function getFirstTown( $postcode ){
					if( !preg_match( '/^\d{4}$/', $postcode ) ) return false;
					$locations = $this->getLocations();
					return trim($locations[$postcode][0][0]);
				}
							
				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param mixed $package
				 * @return void
				 */
				
				public function calculate_shipping( $package ) {
					alog('$package',$package,__FILE__,__LINE__);
					
					//wp_mail( 'team1.aspk@gmail.com', 'Shipping Test', $package );
					//prepare request ($package);
					//if(empty ($package['destination']['city'])) return; 
					$request = $this->prepare_tnt_req($package);
					alog('$request',$request,__FILE__,__LINE__);
					$response = $this->send_tnt_req($request);
					$xml = $response['body'];
					$xml_convert_object = simplexml_load_string($xml);
					$xml_convert = $xml_convert_object->ratedTransitTimeResponse->ratedProducts->ratedProduct;
					if (count($xml_convert) > 0){
						foreach($xml_convert as $xml_cnv){
							$via_shipping = (string)$xml_cnv->product->description;
							$amount =  (string)$xml_cnv->quote->price;
							
							//echo $via_shipping.'<br>'.$amount;
							$rate = array(
							'id' => sanitize_title($via_shipping),
							'label' => $via_shipping,
							'cost' => $amount,
							'calc_tax' => 'per_item'
							);
							$this->add_rate( $rate );
						}
					}
					
				}
			}
		}
	}
	add_action( 'woocommerce_shipping_init', 'WC_Aspk_TNT_init' );
 
	function add_WC_Aspk_TNT( $methods ) {
		$methods[] = 'WC_Aspk_TNT';
		return $methods;
	}
 
	add_filter( 'woocommerce_shipping_methods', 'add_WC_Aspk_TNT' );
}

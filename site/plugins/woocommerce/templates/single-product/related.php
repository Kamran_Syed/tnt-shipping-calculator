<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

$related = $product->get_related( $posts_per_page );
// echo "<pre>"; print_r($related);

if ( sizeof( $related ) == 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->id )
) );

 $products = new WP_Query( $args );
// echo "<pre>"; print_r($products);
$woocommerce_loop['columns'] = $columns; ?>

	<div class="product-right-slider">
		<div class="product-slider-logo">
		   <img src="<?php echo get_template_directory_uri(); ?>/images/product-logo.jpg" />
		</div>
		
		<?php if ( $products->have_posts() ) : ?>

		<div class="product-slider-cont">

			<h1><?php _e( 'Related Products', 'woocommerce' ); ?></h1>
			
			<div class="product-slider-holder">
				<div class="product-slider-inner">
				
					<ul class="relate_slider">

					<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				<div class="product-slider-inner-box">	
					<li>
						<a href="<?php the_permalink(); ?>">
						<?php //do_action( 'woocommerce_before_shop_loop_item_title' ); 
						  if ( has_post_thumbnail() ) {
							the_post_thumbnail('medium'); 
						}?>
						</a><h1><?php the_title(); ?></h1>			
					</li>
				</div>	
				
				<?php endwhile; // end of the loop. ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<?php endif;

	wp_reset_postdata(); ?>
	
    <div class="product-butoncont">
        <a class="download_broc" href="<?php echo get_site_url(); ?>/policies">
			<div class="delivery-img">
				<img alt="delivery" src="<?php echo get_template_directory_uri(); ?>/images/truck.jpg">
				<div style="clear:both;"></div>
			</div>
             <div class="delivery-text">
				Delivery & Pickup
			</div>
		</a>
				  
		<a class="download_broc" href="http://www.chairforce.com.au/links/chairforcebrochure.pdf">
			<div class="broc-img">
				<img alt="brochure" src="<?php echo get_template_directory_uri(); ?>/images/downloadbrochure.png">
			</div>
		    <div class="broc-text">      
				Click here to Download our Brochure
			</div> 
		</a>
    </div>


<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $product;

if ( ! $product->is_purchasable() ) return;
?>

<?php
	// Availability
	$availability = $product->get_availability();

	if ( $availability['availability'] )
		echo apply_filters( 'woocommerce_stock_html', '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>', $availability['availability'] );
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" method="post" enctype='multipart/form-data'>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
	
	 	<p>Quantity</p>
		<?php
	 		if ( ! $product->is_sold_individually() )
	 			woocommerce_quantity_input( array(
	 				'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
	 				'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
	 			) );
				$title=$product->post->post_title;
				$content=$product->post->post_excerpt;
				$permalink = get_permalink( $product->id); 
				$post_thumbnail_id = get_post_thumbnail_id( $product->id ,'small' );
                $url=wp_get_attachment_url($post_thumbnail_id);
				$permalink = get_permalink( $product->id  );
	 	?>

	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
	 	
		<?php //echo "<pre>"; print_r($product); ?>

	 	<button type="submit" data-product_id="<?php echo esc_attr( $product->id ); ?>" data-excerpt="<?php echo esc_attr( $content ); ?>" data-thumbnail="<?php echo esc_attr( $url ); ?>" data-product-title= "<?php echo esc_attr( $title ); ?>"   data-link="<?php echo esc_attr( $permalink ); ?>" class="single_add_to_cart_button button alt"><?php if(is_product_category()){ echo "Buy Now"; } else{ echo $product->single_add_to_cart_text();} ?>  </button>

		<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
		<div id="popup" style="display:block;">
					
				</div>

	</form>


	<?php //do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
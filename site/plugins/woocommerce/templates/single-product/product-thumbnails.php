<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.3
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	?>
	<div class="product-small-img-cont thumbnails"><?php

		$loop = 0;
		$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );

		foreach ( $attachment_ids as $attachment_id ) {

			$classes = array( 'zoom' );

			if ( $loop == 0 || $loop % $columns == 0 )
				$classes[] = 'first';

			if ( ( $loop + 1 ) % $columns == 0 )
				$classes[] = 'last';

			$image_link = wp_get_attachment_url( $attachment_id );

			if ( ! $image_link )
				continue;

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$image_class = esc_attr( implode( ' ', $classes ) );
			$image_title = esc_attr( get_the_title( $attachment_id ) );
			
			if(is_product_category()){ 
		    $image_class="grouped-$post->ID";
			$class="group";
			$image_title = esc_attr( get_the_title( $attachment_id ) );
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a class="%s" title="%s" href="%s" rel="%s" id="post-'.$post->ID.'">%s</a>',$class , $image_title, $image_link,$image_class, $image ), $attachment_id, $post->ID, $image_class );
			// echo $post->ID;
			 // print_r($image);print_r($image_link);
		     // esc_attr( $post->id ); 
			 $loop++;
			}
			   
			if(is_product()){
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );

			$loop++;
			}
		}

	?></div>
	<?php
}
<?php
/**
 * Loop Add to Cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;
  // echo "<pre>"; print_r($product); die();
$post_thumbnail_id = get_post_thumbnail_id( $product->id ,'small' );
$url=wp_get_attachment_url($post_thumbnail_id);
$title=$product->post->post_title;
$content=$product->post->post_excerpt;
$permalink = get_permalink( $product->id  );

echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-thumbnail="%s" data-product-title="%s" data-excerpt="%s" data-link="%s" class="button %s product_type_%s">%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr($url),
		esc_attr($title),
		esc_attr($content),
		esc_attr($permalink),
		$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
		esc_attr( $product->product_type ),
		esc_html( $product->add_to_cart_text() )
	),
$product );

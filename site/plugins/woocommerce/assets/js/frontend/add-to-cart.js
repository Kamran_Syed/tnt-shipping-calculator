jQuery( function( $ ) {

	// wc_add_to_cart_params is required to continue, ensure the object exists
	if ( typeof wc_add_to_cart_params === 'undefined' )
		return false;	
 
	 var image;
	 var content;
	 var link;
	 
	// Ajax add to cart
	$( document ).on( 'click', '.add_to_cart_button', function(e) {
	  e.preventDefault();
	  //alert("hi");
		 $("#popup").show();
		  $("#popup").fadeOut(10000); 	
		
		// AJAX add to cart request
		var $thisbutton = $( this );

		// if ( $thisbutton.is( '.product_type_simple' ) ) {

			if ( ! $thisbutton.attr( 'data-product_id' ) )
				return true;

			$thisbutton.removeClass( 'added' );
			$thisbutton.addClass( 'loading' );
			
			image=$thisbutton.attr('data-thumbnail' );
			content=$thisbutton.attr('data-product-title' );
			link=$thisbutton.attr('data-link' );
			//alert(image);
			var data = {
				action: 'woocommerce_add_to_cart',
				product_id: $thisbutton.attr( 'data-product_id' ),
				quantity: $thisbutton.attr( 'data-quantity' )
			};

			// Trigger event
			$('body' ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

			// Ajax action
			$.post( wc_add_to_cart_params.ajax_url, data, function( response ) {

				if ( ! response )
					return;

				var this_page = window.location.toString();

				this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );
				 //alert(response.product_url);
			
				if ( response.error && response.product_url ) {
					window.location = response.product_url;
					return;
				}

				// Redirect to cart option
				if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {

					window.location = wc_add_to_cart_params.cart_url;
					return;

				} else {

					$thisbutton.removeClass( 'loading' );

					 fragments = response.fragments;
					 cart_hash = response.cart_hash;

					// Block fragments class
					if ( fragments ) {
						$.each( fragments, function( key, value ) {
							$( key ).addClass( 'updating' );
						});
					}

					// Block widgets and fragments
					$( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.6' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6 } } );
					
					// Changes button classes
					$thisbutton.addClass( 'added' );

					// View cart text
					 if ( ! wc_add_to_cart_params.is_cart && $thisbutton.parent().find( '.added_to_cart' ).size() === 0 ) {
					 // alert(image);  
					   var add_carthtm= add_html(image,content,link);
					   jQuery( "#popup" ).html(add_carthtm);					 
						// $thisbutton.after( ' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' + 
							// wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
							 $(".noty_close").click(function(){
						   $("#popup").hide(); 
						 });
						 // $("#popup").fadeOut(100); 	 
					} 

					// Replace fragments
					if ( fragments ) {
						$.each( fragments, function( key, value ) {
							$( key ).replaceWith( value );
						});
					}

					// Unblock
					$( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();

					// Cart page elements
					$( '.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *', function() {

						$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" id="add1" class="plus" />' ).prepend( '<input type="button" value="-" id="minus1" class="minus" />' );

						$( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();

						$( 'body' ).trigger( 'cart_page_refreshed' );
					});

					$( '.cart_totals' ).load( this_page + ' .cart_totals:eq(0) > *', function() {
						$( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
					});

					// Trigger event so themes can refresh other areas
					$( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
				}
			});

			return false;

		// }

		return true;
	});
	
	// Script For empty Cart in Mini cart tab
	$( document ).on( 'click', '#clear-cart', function() {
	
			var empty_cart = $(this).val();
			
			// Ajax action
			var data = {
				action: 'woocommerce_clear_cart_url',
				empty_cart: empty_cart
			};
			
			$.post(wc_add_to_cart_params.ajax_url, data, function( response ) {
			
				$('.widget_shopping_cart_content').text('No products in the cart.');
				$('.cart-parent .amount').text('$0');
				$('.cart-parent .contents').text('0 items');
			});											   
		});
		
	function add_html(image,content,link){
	// alert("hiii");
	var htm='<ul class="noty_cont noty_layout_topRight"><li><div class="noty_bar s_notify noty_layout_topRight noty_alert noty_closable"><div class="noty_message"><span class="noty_text"><h2 class="s_icon_24">Product added to cart</h2><div class="s_item s_size_1 clearfix"><div class="pop-thumb"><a href="" class="s_thumb"><img src="'+image+'" height="85px" width="78px"></a></div><div class="pop-title"><h3><a href="'+link+'"><strong>'+content+'</strong></a><br> added to shopping cart</h3></div><div class="clear-style"></div></div></span><div class="noty_close" ></div></div></div></li></ul>';
	return htm;
	
	}
});

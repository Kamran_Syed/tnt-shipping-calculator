jQuery( function( $ ) {
// Ajax add to cart
	 var image;
	 var content;
	 var link;

    $( document ).on( 'click', '.single_add_to_cart_button ', function() {
         // alert("hello");
		$("#popup").show();
		$("#popup").fadeOut(10000);
		
		var $thisbutton = $( this );
		var quantity = $(".qty").val();
		var product_id = $( "input[name*='add-to-cart']" ).val();		
        $variation_form = $( this ).closest( '.variations_form' );
        var var_id = $variation_form.find( 'input[name=variation_id]' ).val();
        var att_grind = $variation_form.find( 'select[name=attribute_pa_color]' ).val();
        var att_size = $variation_form.find( 'select[name=attribute_pa_size]' ).val();
		image=$thisbutton.attr('data-thumbnail' );
		// alert(image);
		content=$thisbutton.attr('data-product-title' );
		// alert(content);
		link=$thisbutton.attr('data-link' );
        // var att_size = $variation_form.find( 'select[name=attribute_size]' ).val();
        
        // AJAX add to cart request
 
        if ( $thisbutton.is( '.single_add_to_cart_button ' ) ) {
 
            if ( ! $thisbutton.attr( 'data-product_id' ) )
                return true;
 
            $thisbutton.removeClass( 'added' );
            $thisbutton.addClass( 'loading' );
 
            var data = {
                action: 'woocommerce_add_to_cart_variable_rc',
                product_id: $thisbutton.attr( 'data-product_id' ),
                quantity: quantity,
                variation_id: var_id,
                variation: { Colour: att_grind, Size:att_size}
            };
 
            // Trigger event
            $( 'body' ).trigger( 'adding_to_cart', [ $thisbutton, data ] );
 
            // Ajax action
            $.post( wc_add_to_cart_params.ajax_url, data, function( response ) {
 
                if ( ! response )
                    return;
 
                var this_page = window.location.toString();
 
                this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );
 
                if ( response.error && response.product_url ) {
                    window.location = response.product_url;
                    return;
                }
 
                // Redirect to cart option
                if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
 
                    window.location = wc_add_to_cart_params.cart_url;
                    return;
 
                } else {
 
                    $thisbutton.removeClass( 'loading' );
 
                    fragments = response.fragments;
                    cart_hash = response.cart_hash;
 
                    // Block fragments class
                    if ( fragments ) {
                        $.each( fragments, function( key, value ) {
                            $( key ).addClass( 'updating' );
                        });
                    }
 
                    // Block widgets and fragments
                    $( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.6' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6 } } );
 
                    // Changes button classes
                    $thisbutton.addClass( 'added' );
 
                    // View cart text
					if ( ! wc_add_to_cart_params.is_cart && $thisbutton.parent().find( '.added_to_cart' ).size() === 0 ) {
						// alert("hello");
						// alert(response);
					var add_carthtm= add_html(image,content,link);
					   jQuery( "#popup" ).html(add_carthtm);
					 // $thisbutton.after( ' <a class="added_to_cart wc-forward" title="' + wc_add_to_cart_params.i18n_view_cart + '" href="' + wc_add_to_cart_params.cart_url + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
					 $(".noty_close").click(function(){
						   $("#popup").hide(); 
						 });		
					}
 
                    // Replace fragments
                    if ( fragments ) {
                        $.each( fragments, function( key, value ) {
                            $( key ).replaceWith( value );
                        });
                    }
 
                    // Unblock
                    $( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();
 
                    // Cart page elements
                    $( '.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *', function() {
 
                        $( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input id="add1" class="plus" type="button" value="+" />' ).prepend( '<input id="minus1" class="minus" type="button" value="-" />' );
 
                        $( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();
 
                        $( 'body' ).trigger( 'cart_page_refreshed' );
                    });
 
                    $( '.cart_totals' ).load( this_page + ' .cart_totals:eq(0) > *', function() {
                        $( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
                    });
 
                    // Trigger event so themes can refresh other areas
                    $( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
                }
            });
 
            return false;
 
        }
 
        return true;
    });
	
	function add_html(image,content,link){
	 // alert("hello");
	var htm='<ul class="noty_cont noty_layout_topRight"><li><div class="noty_bar s_notify noty_layout_topRight noty_alert noty_closable"><div class="noty_message"><span class="noty_text"><h2 class="s_icon_24">Product added to cart</h2><div class="s_item s_size_1 clearfix"><div class="pop-thumb"><a href="" class="s_thumb"><img src="'+image+'" height="85px" width="78px"></a></div><div class="pop-title"><h3><a href="'+link+'"><strong>'+content+'</strong></a><br> added to shopping cart</h3></div><div class="clear-style"></div></span><div class="noty_close" ></div></div></div></li></ul>';
	return htm;
	
	}
});